﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Model
{
    public class Track
    {
        public string Name { get; set; }
        public LinkedList<Section> Sections { get; set; }

        public Track(string name, SectionTypes[] sections)
        {
            //Make array into linkedlist before making object
            Sections = SectionArrayToList(sections);
            Name = name;
        }

        //Make array into linkedlist
        public LinkedList<Section> SectionArrayToList(SectionTypes[] _sectionTypes)
        {
            LinkedList<Section> sectionList = new LinkedList<Section>();
            foreach (SectionTypes sectionType in _sectionTypes)
            {
                Section section = new Section();
                section.SectionType = sectionType;
                sectionList.AddLast(section);
            }
            return sectionList;
        }
    }
}
