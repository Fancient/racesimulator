﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;

namespace Controller
{
    public static class Data
    {
        public static Competition Competition { get; set; }
        public static Race CurrentRace { get; set; }

        public static void Initialize()
        {
            Competition = new Competition();
            AddStartParticipants();
            AddStartTracks();
            CurrentRace = new Race(Competition.Tracks.Dequeue(), Competition.Participants);
        }

        //Add new participants to start the competition with
        public static void AddStartParticipants()
        {
            Car car1 = new Car();
            Car car2 = new Car();
            Car car3 = new Car();
            Data.Competition.Participants.Add(new Driver("Henk", TeamColours.Red, car1, 0));
            Data.Competition.Participants.Add(new Driver("Bob", TeamColours.Green, car2, 0));
            Data.Competition.Participants.Add(new Driver("Jan", TeamColours.Blue, car3, 0));
        }

        //Add new tracks to start the competition with
        public static void AddStartTracks()
        {
            SectionTypes[] sectionTypesTr1 = {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Finish
            };
            SectionTypes[] sectionTypesTr2 =
            {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Finish
            };
            SectionTypes[] sectionTypesTr3 =
            {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Finish
            };

            Data.Competition.Tracks.Enqueue(new Track("1", sectionTypesTr1));
            Data.Competition.Tracks.Enqueue(new Track("2", sectionTypesTr2));
            Data.Competition.Tracks.Enqueue(new Track("3", sectionTypesTr3));
        }

        //Check if track queue isn't empty -> go to next one in queue
        public static void NextRace()
        {
            Track nextTrack = Competition.NextTrack();

            if(nextTrack != null)
            {
                CurrentRace = new Race(nextTrack, Competition.Participants);
            }
        }

        public static void OnRaceFinished(object sender, RaceFinishedEventArgs e)
        {
            NextRace();
            if(CurrentRace != null)
            {
                CurrentRace.Start();
            }
        }
    }
}
