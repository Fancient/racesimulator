﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Model
{
    public enum TeamColours
    {
        Red,
        Green,
        Yellow,
        Grey,
        Blue
    }

    public interface IParticipant
    {
        public string Name { get; set; }
        public int Points { get; set; }
        public int LapsFinished { get; set; }
        public IEquipment Equipment { get; set; }
        public TeamColours TeamColour { get; set; }
    }
}
