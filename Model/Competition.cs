﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Competition
    {
        public List<IParticipant> Participants = new List<IParticipant>();
        public Queue<Track> Tracks = new Queue<Track>();
        
        //Check if queue isn't empty -> Go to next track
        public Track NextTrack()
        {
            try
            {
                return Tracks.Dequeue();
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}
