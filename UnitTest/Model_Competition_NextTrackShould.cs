﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Model;
using Controller;

namespace UnitTest1
{
    [TestFixture]
    class Model_Competition_NextTrackShould
    {
        private Competition _competition;

        [SetUp]
        public void SetUp()
        {
            _competition = new Competition();
        }

        [Test]
        public void NextTrack_EmptyQueue_ReturnNull()
        {
            Track result = _competition.nextTrack();
            Assert.IsNull(result);
        }
    }
}
