﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
        public enum SectionTypes
        {
            Straight,
            //StraightUp,
            LeftCorner,
            //LeftDownCorner,
            RightCorner,
            //RightDownCorner,
            StartGrid,
            //StartGridVer,
            Finish,
            //FinishVer,
        }
}
