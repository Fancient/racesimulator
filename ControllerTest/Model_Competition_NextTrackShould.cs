﻿using Model;
using NuGet.Frameworks;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ControllerTest
{
    [TestFixture]
    class Model_Competition_NextTrackShould
    {
        private Competition _competition;

        [SetUp]
        public void SetUp()
        {
            _competition = new Competition();
        }

        [Test]
        //Check if an empty queue returns null.
        public void NextTrack_EmptyQueue_ReturnNull()
        {
            Track result = _competition.NextTrack();
            Assert.IsNull(result);
        }

        //Check if method returns single track in queue.
        public void NextTrack_OneInQueue_ReturnTrack()
        {
            SectionTypes[] sectionTypes = new SectionTypes[1];
            Track track = new Track("Track 1", sectionTypes);
            _competition.Tracks.Enqueue(track);

            Track result = _competition.NextTrack();
            Assert.AreEqual(track, result);
        }

        //Check if method removes track from queue
        public void NextTrack_OneInQueue_RemoveTrackFromQueue()
        {
            SectionTypes[] sectionTypes = new SectionTypes[1];
            Track track = new Track("Track 1", sectionTypes);

            Track result = _competition.NextTrack();
            result = _competition.NextTrack();

            Assert.IsNull(result);
        }

        //Check if method returns tracks in correct order
        public void NextTrack_TwoInQueue_ReturnNextTrack()
        {
            //Create 2 tracks and add to queue in competition
            SectionTypes[] sectionTypes = new SectionTypes[2];
            Track track = new Track("Track 1", sectionTypes);
            Track track2 = new Track("Track 2", sectionTypes);

            _competition.Tracks.Enqueue(track);
            _competition.Tracks.Enqueue(track2);

            //Add two results to tracks queue
            Track result = _competition.NextTrack();
            Track result2 = _competition.NextTrack();

            //Check if correct order
            Assert.AreEqual(track, result);
            Assert.AreEqual(track2, result2);
        }
    }
}
