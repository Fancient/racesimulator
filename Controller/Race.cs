﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Controller
{
    public class Race
    {
        public Track Track { get; set; }
        public List<IParticipant> Participants { get; set; }
        public DateTime StartTime { get; set; }
        public int ParticipantsOnTrack;

        public Dictionary<Section, SectionData> _positions;
        private Random _random = new Random(DateTime.Now.Millisecond);

        //Timer initializer
        private Timer timer;

        //Event for drivers
        public event EventHandler<DriversChangedEventArgs> DriversChanged;

        //Event for next race
        public event EventHandler<RaceFinishedEventArgs> RaceFinished;

        //Eventhandler timer
        public void OnTimedEvent(object source, EventArgs e)
        {
            MoveParticipants();
        }

        //When drivers actually move, run this
        public void OnDriversChanged()
        {
            DriversChanged?.Invoke(this, new DriversChangedEventArgs() { Track = Data.CurrentRace.Track });
        }

        public Race(Track track, List<IParticipant> participants)
        {
            _positions = new Dictionary<Section, SectionData>();
            this.Track = track;
            this.Participants = participants;
            ParticipantsOnTrack = participants.Count;
            RandomizeEquipment();
            PlaceParticipants(track, participants);

            timer = new Timer(500);
            timer.Elapsed += OnTimedEvent;
        }

        //Find section and data pairs for each
        public SectionData GetSectionData(Section section)
        {
            SectionData sectionData;

            foreach (KeyValuePair<Section, SectionData> pair in _positions)
            {
                if (EqualityComparer<Section>.Default.Equals(pair.Key, section))
                {
                    sectionData = pair.Value;
                    return sectionData;
                }
            }

            SectionData newSectionData = new SectionData();
            _positions.Add(section, newSectionData);
            return newSectionData;
        }

        //Start timer
        public void Start()
        {
            timer.Start();
        }

        //Set random values for equipment
        public void RandomizeEquipment()
        {
            foreach (IParticipant participant in Participants)
            {
                //TO DO:
                participant.Equipment.Speed = _random.Next(100, 150);
                participant.Equipment.Quality = _random.Next(1, 10);
                participant.Equipment.Performance = _random.Next(1, 3);
            }
        }

        public void PlaceParticipants(Track track, List<IParticipant> participants)
        {
            //Start on startgrid
            LinkedListNode<Section> currentSection = Track.Sections.First;
            GetSectionData(currentSection.Value);

            foreach (IParticipant participant in participants)
            {
                //Place participant on left & right
                if (GetSectionData(currentSection.Value).Left == null)
                {
                    GetSectionData(currentSection.Value).Left = participant;
                }
                else if (GetSectionData(currentSection.Value).Right == null)
                {
                    GetSectionData(currentSection.Value).Right = participant;
                }
                //Check if both are full - continue to next section
                else
                {
                    currentSection = currentSection.Next;
                    GetSectionData(currentSection.Value).Left = participant;
                }
            }
        }

        public enum Side
        {
            Left,
            Right
        }

        public int GetSpeedParticipant(IParticipant participant)
        {
            int speed = participant.Equipment.Speed;
            int performance = participant.Equipment.Performance;
            return speed * performance;
        }

        public SectionData GetNextSectionData(LinkedListNode<Section> CurrentSectionNode)
        {
            SectionData NextSectionData = null;
            SectionData sectionData = GetSectionData(CurrentSectionNode.Value);

            //Check if any participants are left
            if (ParticipantsOnTrack == 0)
            {
                RaceEndEventCall();
            }
            else
            {
                //If participants have reached finish
                if (CurrentSectionNode.Value.SectionType == SectionTypes.Finish)
                {
                    if (sectionData.Left != null)
                    {
                        //Check left participant
                        if (sectionData.Left.LapsFinished < 1)
                        {
                            sectionData.Left.LapsFinished++;

                            //If section is first, go back to last
                            if (CurrentSectionNode == Track.Sections.Last)
                            {
                                NextSectionData = GetSectionData(Track.Sections.First.Value);
                            }
                        }
                        else if (sectionData.Left.LapsFinished == 1)
                        {
                            sectionData.Left.LapsFinished = 0;
                            ParticipantsOnTrack--;
                            sectionData.Left = null;
                        }
                    }

                    if (sectionData.Right != null)
                    {
                        //Check right participant
                        if (sectionData.Right.LapsFinished < 1)
                        {
                            sectionData.Right.LapsFinished++;

                            //If section is first, go back to last
                            if (CurrentSectionNode == Track.Sections.Last)
                            {
                                NextSectionData = GetSectionData(Track.Sections.First.Value);
                            }
                        }
                        else if (sectionData.Right.LapsFinished == 1)
                        {
                            sectionData.Right.LapsFinished = 0;
                            ParticipantsOnTrack--;
                            sectionData.Right = null;
                        }
                    }
                }
                else
                {
                    NextSectionData = GetSectionData(CurrentSectionNode.Next.Value);
                }
            }
            return NextSectionData;
        }

        public void MoveParticipants()
        {
            LinkedListNode<Section> CurrentSectionNode = Track.Sections.Last;

            while (CurrentSectionNode != null)
            {
                if (_positions.ContainsKey(CurrentSectionNode.Value))
                {
                    SectionData CurrentSectionData = GetSectionData(CurrentSectionNode.Value);

                    //Move left participant
                    if (CurrentSectionData.Left != null)
                    {
                        CurrentSectionData.DistanceLeft += GetSpeedParticipant(CurrentSectionData.Left);
                        int lengthSection = 200;

                        //Only move if distance is far enough
                        if (CurrentSectionData.DistanceLeft > lengthSection)
                        {
                            SectionData NextSectionData = GetNextSectionData(CurrentSectionNode);

                            if(NextSectionData == null)
                            {
                                continue;
                            }
                            //If left is null, go to left
                            if (NextSectionData.Left == null)
                            {
                                IParticipant LeftParticipant = CurrentSectionData.Left;
                                //Take participant off current section
                                CurrentSectionData.Left = null;
                                CurrentSectionData.DistanceLeft = 0;
                                //Add participant to next section
                                NextSectionData.Left = LeftParticipant;
                                OnDriversChanged();
                            }
                            //If left is full, right isn't; go to right
                            else if (NextSectionData.Right == null)
                            {
                                IParticipant LeftParticipant = CurrentSectionData.Left;
                                //Take participant off current section
                                CurrentSectionData.Left = null;
                                CurrentSectionData.DistanceLeft = 0;
                                //Add participant to next section
                                NextSectionData.Right = LeftParticipant;
                                OnDriversChanged();
                            }
                            else
                            {
                                //Driver stays where he is
                            }
                        }
                    }

                    if (CurrentSectionData.Right != null)
                    {
                        CurrentSectionData.DistanceRight += GetSpeedParticipant(CurrentSectionData.Right);
                        int lengthSection = 200;

                        //Only move if distance is far enough
                        if (CurrentSectionData.DistanceRight > lengthSection)
                        {
                            SectionData NextSectionData = GetNextSectionData(CurrentSectionNode);

                            if (NextSectionData == null)
                            {
                                continue;
                            }
                            //If right is null, go to right
                            if (NextSectionData.Right == null)
                            {
                                IParticipant RightParticipant = CurrentSectionData.Right;
                                //Take participant off current section
                                CurrentSectionData.Right = null;
                                CurrentSectionData.DistanceRight = 0;
                                //Add participant to next section
                                NextSectionData.Right = RightParticipant;
                                OnDriversChanged();
                            }
                            //If right is full, left isn't; go to left
                            else if (NextSectionData.Left == null)
                            {
                                IParticipant RightParticipant = CurrentSectionData.Right;
                                //Take participant off current section
                                CurrentSectionData.Right = null;
                                CurrentSectionData.DistanceRight = 0;
                                //Add participant to next section
                                NextSectionData.Left = RightParticipant;
                                OnDriversChanged();
                            }
                            else
                            {
                                //Driver stays where he is
                            }
                        }
                    }
                }

                CurrentSectionNode = CurrentSectionNode.Previous;

            }
        }

        public void RaceEndEventCall()
        {
            timer.Stop();
            RaceFinished?.Invoke(this, new RaceFinishedEventArgs());
        }

        public void RemoveEventsCurrentRace()
        {
            if(DriversChanged != null)
            {
                foreach(EventHandler<DriversChangedEventArgs> driversChanged in DriversChanged.GetInvocationList())
                {
                    DriversChanged -= driversChanged;
                }
            }
            if(RaceFinished != null)
            {
                foreach(EventHandler<RaceFinishedEventArgs> raceFinished in RaceFinished.GetInvocationList())
                {
                    RaceFinished -= raceFinished;
                }
            }
        }
    }
}
