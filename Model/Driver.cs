﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Driver : IParticipant
    {
        public string Name { get; set; }
        public int Points { get; set; }
        public int LapsFinished { get; set; }
        public IEquipment Equipment { get; set; }
        public TeamColours TeamColour { get; set; }

        public Driver(string name, TeamColours teamcolour, IEquipment equipment, int lapsFinished)
        {
            Name = name;
            TeamColour = teamcolour;
            Equipment = equipment;
            LapsFinished = lapsFinished;
        }
    }
}
