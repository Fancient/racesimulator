﻿using System;
using Model;
using Controller;
using System.Threading;
using System.Data.Common;

namespace RaceSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Data.Initialize();
            Data.NextRace();
            Data.CurrentRace.DriversChanged += Visualisation.OnDriversChanged;
            Visualisation.DrawTrack(Data.CurrentRace.Track);
            Data.CurrentRace.Start();

            for (; ; )
            {
                Thread.Sleep(100);
            }
        }
    }
}
