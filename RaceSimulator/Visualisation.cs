﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;

namespace RaceSimulator
{
    static class Visualisation
    {
        static void Initialize()
        {

        }

        #region graphics

        //Strings representing sections to be drawn
        //FINISH
        private static string[] _finish = { "-----", "  #1 ", "  #2 ", "-----" };
        private static string[] _finishUp = { "|   |", "|# #|", "|1 2|", "|   |" };
        //TURNS
        private static string[] _cornerLeftToBottom = { "---\\ ", "   1\\", " 2  |", "\\   |" };
        private static string[] _cornerLeftToTop = { "/   |", " 1  |", "   2/", "---/ " };
        private static string[] _cornerRightToBottom = { " /---", "/2   ", "|  1 ", "|   /" };
        private static string[] _cornerRightToTop = { "|   \\", "|  1 ", "\\2   ", " \\---" };
        //START
        private static string[] _start = { "-----", "1>  ", "  2> ", "-----" };
        //STRAIGHT
        private static string[] _straightUp = { "|   |", "|   |", "|1 2|", "|   |" };
        private static string[] _straight = { "-----", "  1  ", "  2  ", "-----" };

        #endregion

        //Method to visualize track onto console
        static public void DrawTrack(Track track)
        {
            (int lowestX, int lowestY) coordinates = CalculateStartPosition(track);
            int y = coordinates.lowestY;
            int x = coordinates.lowestX;

            if (y < 0)
            {
                y = System.Math.Abs(y);
            }
            if (x < 0)
            {
                x = System.Math.Abs(x);
            }

            string direction = "east";
            //Go thru given array
            foreach (Section Section in track.Sections)
            {
                //START
                if (Section.SectionType == SectionTypes.StartGrid)
                {
                    DrawSection(x, y, _start, Section);
                    x += 5;
                    direction = "east";
                }
                //STRAIGHT
                if (Section.SectionType == SectionTypes.Straight)
                {
                    if (direction == "east")
                    {
                        DrawSection(x, y, _straight, Section);
                        x += 5;
                    }
                    else if (direction == "west")
                    {
                        DrawSection(x, y, _straight, Section);
                        x -= 5;
                    }
                    else if (direction == "south")
                    {
                        DrawSection(x, y, _straightUp, Section);
                        y += 4;
                    }
                    else if (direction == "north")
                    {
                        DrawSection(x, y, _straightUp, Section);
                        y -= 4;
                    }
                }
                //RIGHT CORNERS
                if (Section.SectionType == SectionTypes.RightCorner)
                {
                    if (direction == "east")
                    {
                        DrawSection(x, y, _cornerLeftToBottom, Section);
                        y += 4;
                        direction = "south";
                    }
                    else if (direction == "south")
                    {
                        DrawSection(x, y, _cornerLeftToTop, Section);
                        x -= 5;
                        direction = "west";
                    }
                    else if (direction == "west")
                    {
                        DrawSection(x, y, _cornerRightToTop, Section);
                        y -= 4;
                        direction = "north";
                    }
                    else if (direction == "north")
                    {
                        DrawSection(x, y, _cornerRightToBottom, Section);
                        x += 5;
                        direction = "east";
                    }
                }
                //LEFT CORNERS
                if (Section.SectionType == SectionTypes.LeftCorner)
                {
                    if (direction == "east")
                    {
                        DrawSection(x, y, _cornerLeftToTop, Section);
                        y -= 4;
                        direction = "north";
                    }
                    else if (direction == "south")
                    {
                        DrawSection(x, y, _cornerRightToTop, Section);
                        x += 5;
                        direction = "east";
                    }
                    else if (direction == "west")
                    {
                        DrawSection(x, y, _cornerRightToBottom, Section);
                        y += 4;
                        direction = "south";
                    }
                    else if (direction == "north")
                    {
                        DrawSection(x, y, _cornerLeftToBottom, Section);
                        x -= 5;
                        direction = "west";
                    }
                }
                //FINISH
                if (Section.SectionType == SectionTypes.Finish)
                {
                    if (direction == "south" || direction == "north")
                    {
                        DrawSection(x, y, _finishUp, Section);
                        break;
                    }
                    if (direction == "east" || direction == "west")
                    {
                        DrawSection(x, y, _finish, Section);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Method to draw section onto the console
        /// </summary>
        /// <param name="x">x-coordinate given</param>
        /// <param name="y">y-coordinate given</param>
        /// <param name="sectionType">section to be drawn</param>
        static public void DrawSection(int x, int y, string[] sectionType, Section section)
        {
            //cursorposition
            int counter = 0;
            SectionData sectionData = Data.CurrentRace.GetSectionData(section);

            for (int i = y; i < y + 4; i++)
            {
                Console.SetCursorPosition(x, i);
                Console.Write(VisualizeParticipants(sectionType[counter], sectionData.Left, sectionData.Right));
                counter++;
            }
        }

        //Calculate start position for when track goes into negative coords.
        static public (int lowestX, int lowestY) CalculateStartPosition(Track track)
        {
            int x = 0;
            int lowestX = 0;
            int y = 0;
            int lowestY = 0;
            string direction = "east";

            foreach (Section Section in track.Sections)
            {
                //START
                if (Section.SectionType == SectionTypes.StartGrid)
                {
                    x += 5;
                    direction = "east";
                    if(x < lowestX)
                    {
                        lowestX = x;
                    }
                    if(y < lowestY)
                    {
                        lowestY = y;
                    }
                }
                //STRAIGHT
                if (Section.SectionType == SectionTypes.Straight)
                {
                    if (direction == "east")
                    {
                        x += 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                    }
                    else if (direction == "west")
                    {
                        x -= 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                    }
                    else if (direction == "south")
                    {
                        y += 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                    }
                    else if (direction == "north")
                    {
                        y -= 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                    }
                }
                //RIGHT CORNERS
                if (Section.SectionType == SectionTypes.RightCorner)
                {
                    if (direction == "east")
                    {
                        y += 4;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "south";
                    }
                    else if (direction == "south")
                    {
                        x -= 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "west";
                    }
                    else if (direction == "west")
                    {
                        y -= 4;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "north";
                    }
                    else if (direction == "north")
                    {
                        x += 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "east";
                    }
                }
                //LEFT CORNERS
                if (Section.SectionType == SectionTypes.LeftCorner)
                {
                    if (direction == "east")
                    {
                        y -= 4;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "north";
                    }
                    else if (direction == "south")
                    {
                        x += 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "east";
                    }
                    else if (direction == "west")
                    {
                        y += 4;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "south";
                    }
                    else if (direction == "north")
                    {
                        x -= 5;
                        if (x < lowestX)
                        {
                            lowestX = x;
                        }
                        if (y < lowestY)
                        {
                            lowestY = y;
                        }
                        direction = "west";
                    }
                }

            }
            return (lowestX, lowestY);
        }

        //Replaces placeholders with participants
        static string VisualizeParticipants(string section, IParticipant left, IParticipant right)
        {
            //Make visualized name into char
            char empty = ' ';

            if (left != null && right != null)
            {
                //Get names participants
                char leftParticipant = left.Name[0];
                char rightParticipant = right.Name[0];

                //Replace values "1, 2" in string with participants
                section = section.Replace('1', leftParticipant);
                section = section.Replace('2', rightParticipant);
                return section;
            }
            else if (left != null && right == null)
            {
                //Get names participants-
                char leftParticipant = left.Name[0];

                //Replace value "2" with empty space
                section = section.Replace('1', leftParticipant);
                section = section.Replace('2', empty);
                return section;
            }
            else if (right != null && left == null)
            {
                //Get names participants-
                char rightParticipant = right.Name[0];

                //Replace value "2" with empty space
                section = section.Replace('1', empty);
                section = section.Replace('2', rightParticipant);
                return section;
            }
            else
            {
                //Replace both values with empty space
                section = section.Replace('1', empty);
                section = section.Replace('2', empty);
                return section;
            }
        }

        public static void OnDriversChanged(object source, DriversChangedEventArgs e)
        {
            DrawTrack(e.Track);
        }
    }
}
